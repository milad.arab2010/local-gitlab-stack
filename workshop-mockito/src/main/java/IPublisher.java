public interface IPublisher {
    public void attach(WeatherWidget o);
    public void detach(WeatherWidget o);
    public void publish();
}
