import java.util.List;

public interface Widget {
    public boolean isInstalled();
    public boolean isMovable();
    public void install(Position positionOnScreen);
    public void uninstall();
    public Position getPosition();
    public Position updatePosition(int x, int y);
}
