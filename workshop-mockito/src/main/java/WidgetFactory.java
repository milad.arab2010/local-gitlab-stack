public class WidgetFactory {

    public Widget getWidget(String widgetType) {
        if (widgetType.equalsIgnoreCase("weather")) {
            return new WeatherWidget();

        } else if (widgetType.equalsIgnoreCase("clock")) {
            return new ClockWidget();
        }
        return null;
    }
}
