import java.util.Timer;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PublishTest {

    @Mock
    HttpConnection httpConnection;

    @Mock
    WeatherWidget weatherWidget;

    @Mock
    Timer timer;

    private Publisher publisher;

    @Before
    public void init() throws Exception {
        MockitoAnnotations.initMocks(this);

        publisher = Publisher.getInstance();
        publisher.setHttpConnection(httpConnection);
        publisher.setTimer(timer);

        when(httpConnection.GET(anyString())).thenReturn("MHD,12,60");
    }

    @Test
    public void poll() {
        publisher.attach(weatherWidget);
        publisher.poll();
        verify(weatherWidget).update(eq("MHD"), any(Weather.class));
    }

    @Test
    public void detach() {
        publisher.attach(weatherWidget);
        publisher.detach(weatherWidget);
        verify(timer).cancel();
    }
}
