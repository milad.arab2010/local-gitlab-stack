import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.IOException;

import static org.junit.Assert.*;
import static org.mockito.ArgumentMatchers.isNull;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(MockitoJUnitRunner.class)
public class ScreenTest {

    @Mock
    WidgetFactory widgetFactory;

    @Mock
    Widget widget;

    @Mock
    ScreenManager screenManager;

    private Screen myScreen;
    private Position initialPosition;

    @Before
    public void init() throws Exception {
        MockitoAnnotations.initMocks(this);

        myScreen = new Screen(screenManager, widgetFactory);
        initialPosition = new Position(1,2);

        when(widgetFactory.getWidget(anyString())).thenReturn(widget);
    }

    @Test
    public void getWidgets() throws IOException {
        Widget widget = myScreen.installWidget("weather", initialPosition);
        assertEquals(this.widget, myScreen.getWidgets().get(0));
    }

    @Test
    public void installWidget() throws IOException {
        when(screenManager.hasSpace(initialPosition)).thenReturn(Boolean.TRUE);
        doNothing().when(screenManager).assignThread(any(Widget.class));

        Widget widget = myScreen.installWidget("weather", initialPosition);

        verify(widget).install(initialPosition);
        verify(screenManager).assignThread(widget);
        assertEquals(1, myScreen.getWidgets().size());
        //assertTrue(widget.isInstalled()); -- It is a bad assert because it is out of scope of this test class
    }

    @Test(expected=IOException.class)
    public void installWidget_ThrowException() throws IOException {
        when(screenManager.hasSpace(initialPosition)).thenReturn(Boolean.TRUE);
        doThrow(IOException.class).when(screenManager).assignThread(any(Widget.class));

        Widget widget = myScreen.installWidget("weather", initialPosition);

        verify(widget, times(0)).install(initialPosition);
        assertEquals(0, myScreen.getWidgets().size());
    }

    @Test
    public void uninstallWidget() throws Exception {
        when(widget.isInstalled()).thenReturn(true);

        myScreen.installWidget("weather", initialPosition);
        Widget widget = myScreen.getWidgets().get(0);

        myScreen.uninstallWidget(widget);

        verify(screenManager).dischargeThread(widget);
        verify(widget).uninstall();
        assertEquals(0, myScreen.getWidgets().size());
    }

    @Test
    public void uninstallWidget_Irrelevant() throws Exception {
        when(widget.isInstalled()).thenReturn(false);

        int cnt = myScreen.getWidgets().size();
        myScreen.uninstallWidget(widget);

        verify(screenManager, times(0)).dischargeThread(widget);
        verify(widget, times(0)).uninstall();
        assertEquals(cnt, myScreen.getWidgets().size());
    }

    @Test(expected=Exception.class)
    public void uninstallWidget_Exception() throws Exception {
        when(widget.isInstalled()).thenReturn(true);
        doThrow(Exception.class).when(screenManager).dischargeThread(any(Widget.class));

        myScreen.uninstallWidget(widget);
    }
}
