import pytest
from pytest_mock import mocker

import sys
import os
myPath = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, myPath + '/../')
import main


@pytest.fixture
def app():
    app = main.create_app(True)
    yield app

@pytest.fixture
def client(app):
    return app.test_client()

@pytest.fixture
def appContext(app):
    return app.app_context()