import sys
import os
myPath = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, myPath + '/../')

import pytest
import json
import uuid

class TestDataRetrieval():
    
    def test_no_uuid(self, client):
        r = client.get("/api/v1/get")
        assert r.status_code == 404

    def test_get_unauthorized_access(self, client):
        r = client.get("/api/v1/get/123")
        data = json.loads(r.data)
        assert r.status_code == 401
        assert isinstance( data, dict )
        assert "error" in data

    def test_get_unauthorized_access_with_wrong_cred(self, client):
        r = client.get("/api/v1/get/123", headers={"secret": "invalid"})
        data = json.loads(r.data)
        assert r.status_code == 401
        assert isinstance( data, dict )
        assert "error" in data

    def test_get_invalid_uuid(self, client):
        r = client.get("/api/v1/get/123", headers={"secret": "MySuperSecretIsHere!"})
        data = json.loads(r.data)
        assert r.status_code == 400
        assert isinstance( data, dict )
        assert "error" in data
        assert data.get("error") == "Invalid UUID"

    def test_put_unauthorized_access(self, client):
        r = client.post("/api/v1/put")
        data = json.loads(r.data)
        assert r.status_code == 401
        assert isinstance( data, dict )
        assert "error" in data

    def test_put_unauthorized_access_with_wrong_cred(self, client):
        r = client.post("/api/v1/put", headers={"secret": "invalid"})
        data = json.loads(r.data)
        assert r.status_code == 401
        assert isinstance( data, dict )
        assert "error" in data

    def test_put_no_data(self, client):
        r = client.post("/api/v1/put", headers={"secret": "MySuperSecretIsHere!"})
        data = json.loads(r.data)
        assert r.status_code == 400
        assert isinstance( data, dict )
        assert "error" in data
        assert data.get("error") == "Invalid data!"

    def test_put_non_json_data(self, client):
        r = client.post("/api/v1/put", headers={"secret": "MySuperSecretIsHere!", "Content-Type": "application/json"}
                              , data="invalid")
        data = json.loads(r.data)
        assert r.status_code == 400
        assert isinstance( data, dict )
        assert "error" in data
        assert data.get("error") == "Invalid data!"
    
    def test_put_get_data(self, client):
        payload = {"myData": "myData"}
        r = client.post("/api/v1/put", headers={"secret": "MySuperSecretIsHere!", "Content-Type": "application/json"}
                              , data=json.dumps(payload))
        response_data = json.loads(r.data)
        print response_data.get("executionUuid")
        
        assert r.status_code == 202
        assert isinstance( response_data, dict )
        assert response_data.get("executionUuid") != None
        
        _uuid = response_data.get("executionUuid")
     
        r = client.get("/api/v1/get/{}".format( _uuid ), headers={"secret": "MySuperSecretIsHere!"})
       
        response_data = json.loads(r.data)

        assert r.status_code == 200
        assert isinstance( response_data, dict )
        assert "data" in response_data
        assert response_data.get("data") == payload
        assert response_data.get("uuid") == _uuid
        
class TestDataCache():
    
    def test_no_key(self, client):
        r = client.get("/api/v1/cache")
        assert r.status_code == 405

    def test_get_unauthorized_access(self, client):
        r = client.get("/api/v1/cache/123")
        data = json.loads(r.data)
        assert r.status_code == 401
        assert isinstance( data, dict )
        assert "error" in data

    def test_get_unauthorized_access_with_wrong_cred(self, client):
        r = client.get("/api/v1/cache/123", headers={"secret": "invalid"})
        data = json.loads(r.data)
        assert r.status_code == 401
        assert isinstance( data, dict )
        assert "error" in data

    def test_put_no_data(self, client):
        r = client.post("/api/v1/cache", headers={"secret": "MySuperSecretIsHere!"})
        data = json.loads(r.data)
        assert r.status_code == 400
        assert isinstance( data, dict )
        assert "error" in data
        assert data.get("error") == "Invalid data!"

    def test_put_non_json_data(self, client):
        r = client.post("/api/v1/cache", headers={"secret": "MySuperSecretIsHere!", "Content-Type": "application/json"}
                              , data="invalid")
        data = json.loads(r.data)
        assert r.status_code == 400
        assert isinstance( data, dict )
        assert "error" in data
        assert data.get("error") == "Invalid data!"
    
    def test_put_get_data(self, client):
        payload = {"username": "username"}
        r = client.post("/api/v1/cache", headers={"secret": "MySuperSecretIsHere!", "Content-Type": "application/json"}
                              , data=json.dumps(payload))
        response_data = json.loads(r.data)        
        assert r.status_code == 200
        assert isinstance( response_data, dict )
        assert response_data.get("expire_time") != None
        assert response_data.get("username") == "username"
     
        r = client.get("/api/v1/cache/{}".format( "username" ), headers={"secret": "MySuperSecretIsHere!"})
       
        response_data = json.loads(r.data)

        assert r.status_code == 200
        assert isinstance( response_data, dict )
        assert response_data.get("username") == "username"


class TestHealthEndpoint():

    def test_json_format(self, client):
        r = client.get("/api/v1/health")
        data = json.loads(r.data)
        assert r.status_code == 200
        assert isinstance( data, dict )
        assert "MongoDB" in data
        assert "connection" in data["MongoDB"]
        assert "server_status" in data["MongoDB"]
        assert "version" in data["MongoDB"]

        assert "Redis" in data
        assert "server_version" in data["Redis"]
        assert "server_mode" in data["Redis"]
        assert "memory_size" in data["Redis"]

