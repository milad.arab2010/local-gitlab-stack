import sys
import os
myPath = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, myPath + '/../')

import pytest
import json
import uuid

class TestGetDataEndpoint():

    def test_no_uuid(self, client):
        r = client.get("/api/v1/get")
        assert r.status_code == 404

    def test_unauthorized_access(self, client, unauthorized):
        r = client.get("/api/v1/get/123")
        data = json.loads(r.data)
        assert r.status_code == 401
        assert isinstance( data, dict )
        assert "error" in data

    def test_unauthorized_access_with_wrong_cred(self, client):
        r = client.get("/api/v1/get/123", headers={"secret": "invalid"})
        data = json.loads(r.data)
        assert r.status_code == 401
        assert isinstance( data, dict )
        assert "error" in data

    def test_authorization_exception(self, client, authorization_exception):
        r = client.get("/api/v1/get/123" , headers={"secret": "valid"})
        data = json.loads(r.data)
        assert r.status_code == 500
        assert isinstance( data, dict )
        assert "error" in data

    def test_invalid_uuid(self, client, authorized):
        r = client.get("/api/v1/get/123", headers={"secret": "valid"})
        data = json.loads(r.data)
        assert r.status_code == 400
        assert isinstance( data, dict )
        assert "error" in data
        assert data.get("error") == "Invalid UUID"

    def test_valid_uuid(self, client, authorized, mongoDb_search_valid):
        _uuid = str(uuid.uuid4())
        r = client.get("/api/v1/get/{}".format( _uuid ), headers={"secret": "valid"})
        assert r.status_code == 200

    def test_validate_response(self, client, authorized, mongoDb_search_valid):
        _uuid = str(uuid.uuid4())
        r = client.get("/api/v1/get/{}".format( _uuid ), headers={"secret": "valid"})

        data = json.loads(r.data)

        assert isinstance( data, dict )
        assert "data" in data
        assert "uuid" in data
        #assert data.get("uuid") == _uuid
        assert r.status_code == 200

class TestPutDataEndpoint():

    def test_unauthorized_access(self, client):
        r = client.post("/api/v1/put")
        data = json.loads(r.data)
        assert r.status_code == 401
        assert isinstance( data, dict )
        assert "error" in data

    def test_unauthorized_access_with_wrong_cred(self, client, unauthorized):
        r = client.post("/api/v1/put", headers={"secret": "invalid"})
        data = json.loads(r.data)
        assert r.status_code == 401
        assert isinstance( data, dict )
        assert "error" in data

    def test_authorization_exception(self, client, authorization_exception):
        r = client.post("/api/v1/put", headers={"secret": "valid"})
        data = json.loads(r.data)
        assert r.status_code == 500
        assert isinstance( data, dict )
        assert "error" in data

    def test_no_data(self, client, authorized):
        r = client.post("/api/v1/put", headers={"secret": "valid"})
        data = json.loads(r.data)
        assert r.status_code == 400
        assert isinstance( data, dict )
        assert "error" in data
        assert data.get("error") == "Invalid data!"

    def test_non_json_data(self, client, authorized):
        r = client.post("/api/v1/put", headers={"secret": "valid", "Content-Type": "application/json"}
                              , data="invalid")
        data = json.loads(r.data)
        assert r.status_code == 400
        assert isinstance( data, dict )
        assert "error" in data
        assert data.get("error") == "Invalid data!"

    def test_valid_data(self, client, authorized, mongoDb_insert):
        vaildPayload = "{}"
        r = client.post("/api/v1/put", headers={"secret": "valid", "Content-Type": "application/json"}
                              , data=vaildPayload)
        data = json.loads(r.data)
        assert r.status_code == 202
        assert isinstance( data, dict )
        assert "executionUuid" in data

        _uuid = data.get("executionUuid")
        _uuid_hex = uuid.UUID(_uuid.replace("-", ""), version=4).hex
        assert _uuid_hex == _uuid.replace("-", "")

class TestCacheDataEndpoint():

    def test_unauthorized_access(self, client):
        r = client.post("/api/v1/cache")
        data = json.loads(r.data)
        assert r.status_code == 401
        assert isinstance( data, dict )
        assert "error" in data

    def test_unauthorized_access_with_wrong_cred(self, client, unauthorized):
        r = client.post("/api/v1/cache", headers={"secret": "invalid"})
        data = json.loads(r.data)
        assert r.status_code == 401
        assert isinstance( data, dict )
        assert "error" in data

    def test_authorization_exception(self, client, authorization_exception):
        r = client.post("/api/v1/cache", headers={"secret": "valid"})
        data = json.loads(r.data)
        assert r.status_code == 500
        assert isinstance( data, dict )
        assert "error" in data

    def test_no_data(self, client, authorized):
        r = client.post("/api/v1/cache", headers={"secret": "valid"})
        data = json.loads(r.data)
        assert r.status_code == 400
        assert isinstance( data, dict )
        assert "error" in data
        assert data.get("error") == "Invalid data!"

    def test_non_json_data(self, client, authorized):
        r = client.post("/api/v1/cache", headers={"secret": "valid", "Content-Type": "application/json"}
                              , data="invalid")
        data = json.loads(r.data)
        assert r.status_code == 400
        assert isinstance( data, dict )
        assert "error" in data
        assert data.get("error") == "Invalid data!"

    def test_valid_data(self, client, authorized, redis_put):
        vaildPayload = "{\"username\": \"username\"}"
        r = client.post("/api/v1/cache", headers={"secret": "valid", "Content-Type": "application/json"}
                              , data=vaildPayload)
        data = json.loads(r.data)
        assert r.status_code == 200
        assert isinstance( data, dict )
        assert "expire_time" in data
      
class TestGetCacheDataEndpoint():

    def test_no_key(self, client):
        r = client.get("/api/v1/cache")
        assert r.status_code == 405

    def test_unauthorized_access(self, client, unauthorized):
        r = client.get("/api/v1/cache/123")
        data = json.loads(r.data)
        assert r.status_code == 401
        assert isinstance( data, dict )
        assert "error" in data

    def test_unauthorized_access_with_wrong_cred(self, client):
        r = client.get("/api/v1/cache/123", headers={"secret": "invalid"})
        data = json.loads(r.data)
        assert r.status_code == 401
        assert isinstance( data, dict )
        assert "error" in data

    def test_authorization_exception(self, client, authorization_exception):
        r = client.get("/api/v1/cache/123" , headers={"secret": "valid"})
        data = json.loads(r.data)
        assert r.status_code == 500
        assert isinstance( data, dict )
        assert "error" in data

    def test_valid_data(self, client, authorized, redis_get):
        _key = "username"
        r = client.get("/api/v1/cache/{}".format( _key ), headers={"secret": "valid"})
        assert r.status_code == 200

class TestHealthEndpoint():

    def test_json_format(self, client, mongoDb_health, redis_health):
        r = client.get("/api/v1/health")
        data = json.loads(r.data)
        assert r.status_code == 200
        assert isinstance( data, dict )
        assert "MongoDB" in data
        assert "connection" in data["MongoDB"]
        assert "server_status" in data["MongoDB"]
        assert "version" in data["MongoDB"]
        
        assert "Redis" in data
        assert "server_version" in data["Redis"]
        assert "server_mode" in data["Redis"]
        assert "memory_size" in data["Redis"]