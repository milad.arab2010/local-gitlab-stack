import redis
from flask import current_app as app

class MemCache():

    """
    Cache temporary data in Redis
    """
    def __init__(self):
        # Use the app config to fetch connection string
        self.client = redis.from_url( app.config.get("REDIS_URI") )

    def put(self, path, data, _expiry):
        self.client.hmset(path, data)
        self.client.expire(path, _expiry)

    def get(self, path):
        return self.client.hgetall(path)

    def health(self):
        return {
                    "server_version": self.client.info().get("redis_version"),
                    "server_mode": self.client.info().get("redis_mode"),
                    "memory_size": self.client.info().get("total_system_memory_human")
               }
