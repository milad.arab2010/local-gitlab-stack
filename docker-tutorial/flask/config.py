import logging

logger = logging.getLogger(__name__)

# remark: whole config logic needs to be rewritten:
# - load base config from file  ( not from class )
# - Check for cf environment and in case: overwrite db
# - Check if test_data was provided and overwrite if given

class BaseConfig(object):

    "Config params valid for all configurations"

    COLLECTION = "MY_COL"
    EXECUTION_TIMEOUT = 1200 # seconds

    DEVELOPMENT = True
    DEBUG = True

    MONGO_URI ="mongodb://localhost:27017/DB1"
    MONGO_DBNAME = "MY_DB"

    REDIS_URI ="redis://localhost:6379"

    SECRET_KEY='asdlskjesqVVVVV!!%lksldfj134556kk8ksk2$'

class ProductionConfig():

    "Configuration when running a local server or in"
    "Cloud Foundry environment"

    COLLECTION = "MY_COL"
    EXECUTION_TIMEOUT = 1200 # seconds

    DEVELOPMENT = False
    DEBUG = False

class PytestConfig(object):

    "Configuration when running from pytest"

    SECRET_KEY ='dev'
    PYTEST=True
